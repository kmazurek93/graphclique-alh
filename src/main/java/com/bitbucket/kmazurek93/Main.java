package com.bitbucket.kmazurek93;

import static com.bitbucket.kmazurek93.graph.GraphProvider.*;
import static java.lang.System.out;

import com.bitbucket.kmazurek93.algo.AnnealingParameters;
import com.bitbucket.kmazurek93.algo.SimAnnealing;
import com.bitbucket.kmazurek93.algo.Solution;
import com.bitbucket.kmazurek93.graph.Graph;

public class Main {

  public static void main(String[] args) {
//    Graph graph = sampleSmall();
//    Graph graph = Kn(10);
    Graph graph = fromResource("hamming6-2.clq");
    SimAnnealing simAnnealing;
    Solution solution;
    for (int cliqueSize = 32; cliqueSize > 1; cliqueSize--) {
      AnnealingParameters parameters = new AnnealingParameters(100, 0.001, 0.9995,
          cliqueSize);
      simAnnealing = new SimAnnealing(graph, parameters);
      simAnnealing.run();
      solution = simAnnealing.getPossibleSolution();
      if (solution.isClique()) {
        out.println("Found solution for clique size " + cliqueSize);
        out.println(solution);
        break;
      } else {
        out.println("There is probably no clique in size " + cliqueSize);
        out.println("Decreasing size by 1.");
      }
    }


  }
}
