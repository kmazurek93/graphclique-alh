package com.bitbucket.kmazurek93.algo;

import com.bitbucket.kmazurek93.graph.Graph;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Note: this class has a natural ordering that is inconsistent with equals
 */
public final class Solution implements Comparable<Solution> {

  private final Set<Integer> solutionVertices;
  private final int fValue;
  private final Graph graph;
  private final boolean clique;

  public boolean isClique() {
    return clique;
  }

  public Solution(Set<Integer> solutionVertices, int fValue,
      Graph graph) {
    this.solutionVertices = solutionVertices;
    this.fValue = fValue;
    this.graph = graph;
    this.clique = isReallyAClique();
  }


  @Override
  public int compareTo(Solution o) {
    return o.fValue - this.fValue;
  }

  @Override
  public String toString() {
    return "Solution{" +
        "solutionVertices=" + solutionVertices +
        ", fValue=" + fValue +
        ", isAClique=" + clique +
        '}';
  }

  private boolean isReallyAClique() {
    boolean isClique = true;
    for (Integer vertex : solutionVertices) {
      Set<Integer> subset = solutionVertices.stream().filter(v -> !Objects.equals(v, vertex))
          .collect(Collectors.toSet());
      for (Integer v : subset) {
        if (graph.adjacency(v, vertex) == 0) {
          isClique = false;
          break;
        }
      }
      if (!isClique) {
        break;
      }
    }
    return isClique;
  }
}
