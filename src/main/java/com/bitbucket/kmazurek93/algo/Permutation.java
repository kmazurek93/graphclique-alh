package com.bitbucket.kmazurek93.algo;

import com.bitbucket.kmazurek93.graph.Graph;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;
import org.apache.commons.math3.util.Pair;

final class Permutation {

  private final int data[];
  private final int v;

  private Permutation(int v, int[] from) {
    data = Arrays.copyOf(from, v);
    this.v = v;
  }

  Permutation(int v, boolean initial) {
    this.v = v;
    data = new int[v];
    if (initial) {
      for (int i = 0; i < v; i++) {
        data[i] = i;
      }
    }
  }

   Permutation ofSwappedVertices(int v1, int v2) {
    Permutation permutation = new Permutation(v, data);
    int t = permutation.data[v1];
    permutation.data[v1] = permutation.data[v2];
    permutation.data[v2] = t;

    return permutation;
  }

  static Permutation ofDegrees(Graph g) {
    int v = g.verticesCount();
    int[] permu = new int[v];
    List<Pair<Integer, Integer>> vertexToDeg = new ArrayList<>();
    IntStream.range(0, v)
        .forEach(i -> vertexToDeg.add(new Pair<>(i, g.deg(i))));
    vertexToDeg.sort(Comparator.comparing(Pair::getValue));
    IntStream.range(0, v)
        .forEach(i -> permu[i] = vertexToDeg.get(i).getKey());
    return new Permutation(v, permu);
  }

  int sigma(int v) {
    return data[v];
  }
}
