package com.bitbucket.kmazurek93.algo;

import com.bitbucket.kmazurek93.graph.Graph;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public final class SimAnnealing {

  private final Graph graph;
  private Permutation currentSolution;
  private AnnealingParameters parameters;

  private double currentTemperature;


  public SimAnnealing(Graph graph, AnnealingParameters parameters) {
    //initializes.
    this.graph = graph;
    this.currentSolution = Permutation.ofDegrees(graph);
    this.parameters = parameters;
    this.currentTemperature = parameters.initialTemperature;
  }

  public void run() {
    while (currentTemperature > parameters.endTemperature) {
      //replacing vertices and state transition
      int fCurrent = F(currentSolution);
      if (fCurrent == 0) {
        break;
      }
      int v = graph.verticesCount();
      int bound = 8 * v;
      int cliqueSize = parameters.cliqueSize;
      int vu, vw;
      Permutation permutation = new Permutation(v, false);
      if (cliqueSize == v) {
        vu = ThreadLocalRandom.current().nextInt(0, cliqueSize - 1);
        vw = ThreadLocalRandom.current().nextInt(cliqueSize - 1, v);
      } else {
        vu = ThreadLocalRandom.current().nextInt(0, cliqueSize);
        vw = ThreadLocalRandom.current().nextInt(cliqueSize, v);
      }
      for (int i = 0; i < bound; i++) {
        permutation = this.currentSolution.ofSwappedVertices(vu, vw);
        if (Fprim(vu) <= Fprim(vw)) {
          break;
        }
      }

      int fNext = F(permutation);

      //if F(G, sigma') == 0 we have clique.
      if (fNext == 0) {
        this.currentSolution = permutation;
        break;
      }

      int deltaF = fNext - fCurrent;

      if (deltaF < 0) {
        this.currentSolution = permutation;
      } else {
        double p = acceptance(deltaF);
        if (p > ThreadLocalRandom.current().nextDouble()) {
          this.currentSolution = permutation;
        }
      }

      performCoolDown();
    }
  }

  public Solution getPossibleSolution() {
    Set<Integer> solution = new HashSet<>();
    IntStream.range(0, parameters.cliqueSize)
        .forEach(i -> solution.add(currentSolution.sigma(i)));
    int f = F(currentSolution);
    return new Solution(solution, f, graph);
  }


  private double acceptance(int deltaF) {
    return Math.pow(Math.E, (-deltaF / currentTemperature));
  }

  private int F(Permutation p) {
    int sum = 0;
    for (int k = 0; k < parameters.cliqueSize - 1; k++) {
      for (int l = k + 1; l < parameters.cliqueSize; l++) {
        sum += (1 - graph.adjacency(p.sigma(k), p.sigma(l)));
      }
    }
    return sum;
  }

  private int Fprim(int vertex) {
    int sum = 0;
    for (int i = 0; i < parameters.cliqueSize; i++) {
      if (currentSolution.sigma(i) != currentSolution.sigma(vertex)) {
        sum += graph.adjacency(currentSolution.sigma(i), currentSolution.sigma(vertex));
      }
    }
    return sum;
  }

  private void performCoolDown() {
    this.currentTemperature = this.currentTemperature * parameters.coolingCoeff;
  }
}
