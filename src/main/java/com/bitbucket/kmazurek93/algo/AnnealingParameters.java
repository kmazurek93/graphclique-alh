package com.bitbucket.kmazurek93.algo;

public final class AnnealingParameters {

  double initialTemperature;
  double endTemperature;
  double coolingCoeff;
  int cliqueSize;

  public AnnealingParameters(double initialTemperature, double endTemperature,
      double coolingCoeff, int cliqueSize) {
    this.initialTemperature = initialTemperature;
    this.endTemperature = endTemperature;
    this.coolingCoeff = coolingCoeff;
    this.cliqueSize = cliqueSize;
  }
}
