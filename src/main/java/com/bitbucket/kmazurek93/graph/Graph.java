package com.bitbucket.kmazurek93.graph;


public final class Graph {

  int[][] adjMatrix;

  private final int v;

  Graph(int vertices) {
    adjMatrix = new int[vertices][vertices];
    v = vertices;
  }

  public int adjacency(int from, int to) {
    return adjMatrix[from][to];
  }

  void setAdjacency(int from, int to) {
    adjMatrix[from][to] = 1;
    adjMatrix[to][from] = 1;
  }

  public int verticesCount() {
    return v;
  }

  public int deg(int vertex) {
    int deg = 0;
    for(int i=0; i<v; i++) {
      deg += adjMatrix[vertex][i];
    }
    return deg;
  }

}
