package com.bitbucket.kmazurek93.graph;

import java.util.Arrays;

public final class GraphProvider {

  private GraphProvider() {
  }

  public static Graph Kn(int n) {
    Graph graph = new Graph(n);
    for (int i = 0; i < n; i++) {
      Arrays.fill(graph.adjMatrix[i], 1);
    }
    return graph;
  }

  public static Graph fromResource(final String resourceName) {
    return GraphReader.readGraph(resourceName);
  }

  public static Graph sampleSmall() {
    Graph graph = new Graph(8);
    graph.setAdjacency(0, 1);
    graph.setAdjacency(0, 2);
    graph.setAdjacency(0, 5);
    graph.setAdjacency(0, 6);
    graph.setAdjacency(1, 0);
    graph.setAdjacency(1, 2);
    graph.setAdjacency(1, 3);
    graph.setAdjacency(1, 5);
    graph.setAdjacency(1, 6);
    graph.setAdjacency(2, 0);
    graph.setAdjacency(2, 1);
    graph.setAdjacency(2, 4);
    graph.setAdjacency(2, 5);
    graph.setAdjacency(2, 6);
    graph.setAdjacency(3, 1);
    graph.setAdjacency(3, 4);
    graph.setAdjacency(3, 6);
    graph.setAdjacency(3, 7);
    graph.setAdjacency(4, 2);
    graph.setAdjacency(4, 6);
    graph.setAdjacency(4, 3);
    graph.setAdjacency(5, 0);
    graph.setAdjacency(5, 1);
    graph.setAdjacency(5, 2);
    graph.setAdjacency(5, 6);
    graph.setAdjacency(5, 7);
    graph.setAdjacency(6, 0);
    graph.setAdjacency(6, 1);
    graph.setAdjacency(6, 2);
    graph.setAdjacency(6, 3);
    graph.setAdjacency(6, 4);
    graph.setAdjacency(6, 5);
    graph.setAdjacency(6, 7);
    graph.setAdjacency(7, 2);
    graph.setAdjacency(7, 3);
    graph.setAdjacency(7, 5);
    graph.setAdjacency(7, 6);
    return graph;
  }

}
