package com.bitbucket.kmazurek93.graph;

import static java.lang.Integer.parseInt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

final class GraphReader {


  public static Graph readGraph(String resourceName) {
    InputStream graphResource = GraphReader.class.getResourceAsStream(resourceName);
    assert graphResource != null : "Cannot read " + resourceName;
    Graph graph;
    String line;
    try (BufferedReader reader = new BufferedReader(new InputStreamReader(graphResource))) {
      line = reader.readLine();
      String[] graphData = line.split(" ", -1);
      int size = parseInt(graphData[2]);
      graph = new Graph(size);
      reader.lines().forEach(l -> {
            String[] edge = l.split(" ", -1);
            int from = parseInt(edge[1]) - 1;
            int to = parseInt(edge[2]) - 1;
            graph.setAdjacency(from, to);
          }
      );
    } catch (IOException ioex) {
      throw new RuntimeException(ioex);
    }
    return graph;
  }
}
